const express=require(`express`)
const db=require('../db')
const utils=require('../utils')
const cryptoJs=require('crypto-js')
const e=require('express')
const { response, request, Router } = require('express')
const { CLIENT_SECURE_CONNECTION } = require('mysql/lib/protocol/constants/client')


const router=express.Router()

router.post('/',(request,response)=>{
    //Title    | Description | Price | Category
    const{Title,Description,Price,Category}=request.body 
    const connection=db.openConnection()
    const statement=`insert into product (Title,Description,Price,Category) values ('${Title}','${Description}','${Price}','${Category}')`
    connection.query(statement,(error,result)=>{
         connection.end()
         response.send(utils.createResult(error,result))
    })
})

router.get('/',(request,response)=>{
    const connection=db.openConnection()
    const statement =`select * from product`
    connection.query(statement,(error,result)=>{
        if(error)
        {
            response.send(utils.createResult(error))
        }
        else if(result.length==0)
        {
            response.send(utils.createResult('user not found'))
        }
        else 
        {
            connection.end()
            response.send(utils.createResult(null,result))
        }
    })
})

router.put('/:id',(request,response)=>{
    const{Title,Description}=request.body
    const {id}=request.params
    const connection=db.openConnection()
    const statement=`update product set Title='${Title}', Description='${Description}' where id='${id}'`
    connection.query(statement,(error,result)=>{
        connection.end()
        response.send(utils.createResult(error,result))
    })
})

router.delete('/:id',(request,response)=>{
    const{id}=request.params
    const statement=`delete from product where id='${id}'`
    const connection=db.openConnection()
    connection.query(statement,(error,result)=>{
        connection.end()
        response.send(utils.createResult(error,result))
    })
})
router.get('/',(request,response)=>{
    const connection=db.openConnection()
    const statement =`select * from product`
    connection.query(statement,(error,result)=>{
        if(error)
        {
            response.send(utils.createResult(error))
        }
        else if(result.length==0)
        {
            response.send(utils.createResult('user not found'))
        }
        else 
        {
            connection.end()
            response.send(utils.createResult(null,result))
        }
    })
})






module.exports=router