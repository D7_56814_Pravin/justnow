const express = require('express')

const cors = require('cors')
//const routerProduct = require('./routes/Product')
const routerProduct =require('./routes/product')
const routeruser=require('./routes/user')

const app = express()
app.use(express.json())

app.use(cors('*'))


app.use('/user',routeruser)
app.use('/product',routerProduct)
//app.use('/Product',routerProduct)





app.listen(4000, () => {
    console.log(`server started on port 4000`)
  })